#!/bin/bash -       
#title           :init.sh
#description     :This script will make a stream from the RSTP protocol
#author          :Andrey SHSH
#date            :25092020
#version         :1.3
#==============================================================================

vlc --ttl 12 -vvv $1 --sout '#duplicate{dst=std{access=http,mux=ts,dst=[::]:3013},dst=std{access=udp,mux=ts,dst=ffe2::1]:2013},dst=std{access=livehttp{seglen=5,delsegs=true,numsegs=5,index=./stream.m3u8,index-url=http://'$2'/stream-########.ts},mux=ts{use-key-frames},dst=./stream-########.ts}}}'